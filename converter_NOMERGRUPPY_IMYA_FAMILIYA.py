# -*- coding: utf-8 -*-

from datetime import datetime, date
from forex_python.converter import CurrencyRates



take_quotes = CurrencyRates()
dict_quotes = take_quotes.get_rates("USD")
get_history = list()

while True:
    while True:
        sale_currency = (input("Веедите трехбуквенную аббревиатуру валюты, которую хотите конвертировать: ")).upper()
        if sale_currency in dict_quotes:
            break
        else:
            print("Неверный ввод, попробуйте еще раз, например: USD")
            continue

    while True:
        purchase_currency = input("Введите трехбуквенную аббревиатуру валюты, в которую хотите конвертировать: ").upper()
        if purchase_currency in dict_quotes:
            break
        else:
            print("Неверный ввод, попробуйте еще раз, например: USD")
            continue

    while True:
        amount_currency = input("Введите количество валюты, которую хотите конвертировать: ")
        try:
            amount_currency = int(amount_currency)
        except ValueError:
            print("Неверный ввод, попробуйте еще раз, используйте цифры от 0 до 9 без специальных символов")
            continue
        else:
            break

    while True:
        date_question = input("Вас интересует курс валют на сегодняшний день или на другую дату (today/other): ")
        if date_question == "other":
            while True:
                your_date = input("Введите дату в формате 'год-месяц-день', чтобы посмотреть курс на указанную дату: ")
                try:
                    your_date = your_date.split("-")
                    year, month, day = list(map(int, your_date))
                    appointment_date = date(year, month, day)
                except ValueError:
                    print("Пожалуйста проверьте формат даты и правильность ввода!")
                    continue
                else:
                    break
            break
        else:
            appointment_date = datetime.now()
            break


    def exchange():
        result = take_quotes.convert(sale_currency, purchase_currency, amount_currency, appointment_date)
        current_rate = take_quotes.get_rate(sale_currency, purchase_currency)
        get_history.append(f"Вы конвертировали {sale_currency} в {purchase_currency} по курсу {current_rate}")
        print(f"\n{amount_currency} {sale_currency} = {result} {purchase_currency}\n")

    exchange()
    exit_question = input("Хотите продолжить? y/n y - да, n - выход: ")
    if exit_question == "y":
        continue
    else:
        break

print("Всего хорошего! До встречи!")
